package dao;

import entity.Product;
import entity.ProductCategory;
import entity.shop.Shop;

import java.sql.SQLException;
import java.util.List;

public interface ProductDAO {

    //create
    void add(Product product) throws SQLException;

    //read
    List<Product> fetchByShop(Shop shop);

    List<Product> fetchByProductCategory(ProductCategory productCategory);

    List<Product> fetchByProductStatus(Shop shop, String status);

    //update
    void update(Product product) throws SQLException;

    //delete
    void remove(Product product) throws SQLException;
}
