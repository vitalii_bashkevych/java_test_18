package dao;

import entity.ProductCategory;
import entity.shop.Shop;

import java.sql.SQLException;
import java.util.List;

public interface ProductCategoryDAO {

    //create
    //void add(ProductCategory productCategory) throws SQLException;

    //read
    List<ProductCategory> fetchByShop(Shop shop) throws SQLException;

    //update
    //void update(ProductCategory productCategory) throws SQLException;

    //delete
    //void remove(ProductCategory productCategory) throws SQLException;
}
