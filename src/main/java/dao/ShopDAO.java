package dao;

import entity.shop.Shop;

public interface ShopDAO {

    //create
    //void add(Shop shop) throws SQLException;

    //read
    Shop fetchShop(Shop shop);

    //update
    //void update(Shop shop) throws SQLException;

    //delete
    //void remove(Shop shop) throws SQLException;
}
