package service;

import dao.ShopDAO;
import entity.shop.Shop;
import connectionSource.ConnectionUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ShopService extends ConnectionUtil implements ShopDAO {

    private static final String TABLE_NAME = "shop";

    private static final String ID = "shop_id";
    private static final String NAME = "shop_name";
    private static final String ADDRESS = "shop_address";
    private static final String PHONE = "contact_phone";

    @Override
    public Shop fetchShop(Shop shop){
        ResultSet rs;
        String sql = "select * from " + TABLE_NAME + " where " + NAME + "=\"" + shop.getName()+"\"";
        //System.out.println("sql query: " + sql);
        rs = getResultSet(sql);

        try{
            //ResultSet rs = new ShopService().fetchByName(GunShop.SHOP_NAME);
            while (rs.next()){
                shop.setId(rs.getInt(ID));
                shop.setName(rs.getString(NAME));
                shop.setAddress(rs.getString(ADDRESS));
                shop.setPhone(rs.getString(PHONE));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return shop;
    }

}
