package service;

import dao.ProductCategoryDAO;
import entity.ProductCategory;
import connectionSource.ConnectionUtil;
import entity.shop.Shop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductCategoryService extends ConnectionUtil implements ProductCategoryDAO {

    private static final String TABLE_NAME = "product_category";

    private static final String ID = "product_category_id";
    private static final String NAME = "product_category_name";
    private static final String SHOP = "shop_id";

    @Override
    public List<ProductCategory> fetchByShop(Shop shop) {
        List<ProductCategory> productCategories = new ArrayList<>();
        ResultSet rs = null;
        String sql = "select * from " + TABLE_NAME + " where " + SHOP + " = " + shop.getId()+"";
        //System.out.println("sql query: " + sql);
        try {
            rs = getResultSet(sql);
            while (rs.next()) {
                ProductCategory productCategory = new ProductCategory();
                productCategory.setId(rs.getInt(ID));
                productCategory.setName(rs.getString(NAME));
                productCategory.setShop(shop);

                productCategories.add(productCategory);
            }
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return productCategories;
    }

}
