package service;

import connectionSource.ConnectionUtil;
import dao.ProductDAO;
import entity.Product;
import entity.ProductCategory;
import entity.shop.Shop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductService extends ConnectionUtil implements ProductDAO {
    //table name
    private static final String TABLE_NAME = "product";

    //column name
    private static final String ID = "product_id";
    private static final String TITLE = "product_title";
    private static final String PRICE = "product_price";
    private static final String SHOP = "shop_id";
    private static final String PRODUCT_CATEGORY = "product_category_id";
    private static final String STATUS = "status";

    @Override
    public void add(Product product) {
        String sql = "insert into product (" + ID + ", " + TITLE+ ", " + PRICE + ", " + SHOP + ", " + PRODUCT_CATEGORY + ", " + STATUS + ")" +
                "values (" +
                    "DEFAULT, " +
                    "\"" + product.getTitle() + "\", " +
                    product.getPrice() + ", " +
                    product.getShop().getId() + ", " +
                    product.getProductCategory().getId() + ", " +
                    "\"" + product.getStatus().toString() + "\"" +
                ")";
        insertToTable(sql);
    }

    @Override
    public List<Product> fetchByShop(Shop shop){
        return null;
    }

    @Override
    public List<Product> fetchByProductCategory(ProductCategory productCategory){
        List<Product> products = new ArrayList<>();
        ResultSet rs = null;
        String sql = "select * from " + TABLE_NAME + " where " + PRODUCT_CATEGORY + " = " + productCategory.getId()+"";
        //System.out.println("sql query: " + sql);
        try {
            rs = getResultSet(sql);
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt(ID));
                product.setTitle(rs.getString(TITLE));
                product.setPrice(rs.getBigDecimal(PRICE));
                product.setShop(productCategory.getShop());
                product.setProductCategory(productCategory);
                product.setStatus(Product.Status.valueOf(rs.getString(STATUS)));

                products.add(product);
            }
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return products;
    }

    @Override
    public List<Product> fetchByProductStatus(Shop shop, String status){
        List<Product> products = new ArrayList<>();
        ResultSet rs = null;
        String sql = "select * from " + TABLE_NAME +
                " where " +
                    STATUS + " = \"" +  status + "\"" +
                    " and " +
                    SHOP + " = " + shop.getId();
        //System.out.println("sql query: " + sql);
        try {
            rs = getResultSet(sql);
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt(ID));
                product.setTitle(rs.getString(TITLE));
                product.setPrice(rs.getBigDecimal(PRICE));
                product.setShop(shop);
                //product.setProductCategory(productCategory);
                product.setStatus(Product.Status.valueOf(rs.getString(STATUS)));

                products.add(product);
            }
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return products;
    }

    @Override
    public void update(Product product) {
        String sql = "UPDATE product " +
                "set " +
                    TITLE + " = \"" + product.getTitle() + "\", " +
                    PRICE + " = " + product.getPrice() + ", " +
                    //SHOP + " = " + product.getShop().getId() + ", " +
                    //PRODUCT_CATEGORY + " = " + product.getProductCategory().getId() + ", " +
                    STATUS + " = \"" + product.getStatus().toString()+ "\"" +
                " WHERE " + ID + "=" + product.getId();
        insertToTable(sql);
    }

    @Override
    public void remove(Product product){
        String sql = "delete from product " +
                " WHERE " + ID + "=" + product.getId();
        insertToTable(sql);
        product = null;
    }


}
