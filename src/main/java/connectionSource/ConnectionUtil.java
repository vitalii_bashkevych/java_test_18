package connectionSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtil {
    private static String DB_NAME = "test_db";
    private static String USER_NAME = "test";
    private static String PWD = "test";
    private static String URL = "jdbc:mysql://localhost/" + DB_NAME + "?user=" + USER_NAME + "&password=" + PWD;
    private static String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static int POOL_SIZE = 2;
    private static ConnectionPool CONN_POOL = new ConnectionPool(URL, DRIVER_NAME, POOL_SIZE);

    private Connection myConnection;
    private Statement statement;

    public ConnectionUtil() {
    }

    protected void insertToTable(String sql) {
        try {
            this.myConnection = CONN_POOL.retrieve();
            this.statement = myConnection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        CONN_POOL.putback(myConnection);
    }

    protected ResultSet getResultSet(String sql) {
        ResultSet res = null;
        try {
            this.myConnection = CONN_POOL.retrieve();
            this.statement = myConnection.createStatement();
            res = statement.executeQuery(sql);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        CONN_POOL.putback(myConnection);
        return res;
    }
}
