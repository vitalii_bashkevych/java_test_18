package entity;

import entity.shop.Shop;

import java.math.BigDecimal;

public class Product {
    private int id;
    private String title;
    private BigDecimal price;
    private Status status;
    private ProductCategory productCategory;
    private Shop shop;

    public Product() {
    }

    public enum Status {
        Available(1), Absent(2), Expected(3);
        private int id;


        Status(int id) {
            this.id = id;
        }

        public int id() {
            return id;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", status=" + status +
                ", myShop=" + shop +
                ", productCategory=" + productCategory +
                '}';
    }
}
