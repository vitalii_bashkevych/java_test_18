package entity.shop;

import java.math.BigDecimal;

import java.util.List;

import entity.Product;
import entity.ProductCategory;

public class BookShop extends Shop {

    private static BookShop bookShop = null;
    public static String SHOP_NAME = "Book Shop";
    private static String bookProdNames[] = {"book_1", "book_2", "book_3", "book_4", "book_5", "book_6", "book_7", "book_8", "book_9", "book_10",};

    private BookShop() {
    }

    public static BookShop getInstance() {
        if (bookShop == null) {
            bookShop = new BookShop();
            bookShop.setName(SHOP_NAME);
        }
        return bookShop;
    }

    @Override
    public void run() {
        List<ProductCategory> productCategories = this.getAllProductCategories();
        populateProductTable(bookProdNames, productCategories, this);

        for (int i = 0; i < productCategories.size(); i++) {
            if (i == 0) {
                List<Product> someProducts = this.getProductsByProductCategory(productCategories.get(i));
                for (Product prod : someProducts) {
                    changProductStatus(prod, Product.Status.Absent.name());
                }
                System.out.println("Products Status were updated with " + Product.Status.Absent.name() + " for " + SHOP_NAME);
            } else {
                List<Product> someProducts = this.getProductsByProductCategory(productCategories.get(i));
                for (int x = 0; x < someProducts.size(); x += 2) {
                    changProductStatus(someProducts.get(x), Product.Status.Expected.name());
                }
                System.out.println("Products Status were updated with " + Product.Status.Expected.name() + " for " + SHOP_NAME);
            }
        }

        List<Product> availableProducts = this.getProductsByProductStatus(Product.Status.Available.name());

        for (Product prod : availableProducts) {
            changProductPrice(prod, new BigDecimal("1.2"));
        }
        System.out.println("Products Price were updated " + Product.Status.Expected.name() + " for " + SHOP_NAME);
        System.out.println(SHOP_NAME + " WAS CLOSED");
        if (latch != null) {
            latch.countDown();
        }
    }
}
