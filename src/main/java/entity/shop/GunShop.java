package entity.shop;

import java.math.BigDecimal;

import java.util.List;

import entity.Product;
import entity.ProductCategory;

public class GunShop extends Shop {

    private static GunShop gunShop = null;
    private static String SHOP_NAME = "Gun Shop";
    private static String gunProdNames[] = {"gun_1", "gun_2", "gun_3", "gun_4", "gun_5", "gun_6", "gun_7", "gun_8", "gun_9", "gun_10", "gun_11"};

    private GunShop() {
    }

    public static GunShop getInstance() {
        if (gunShop == null) {
            gunShop = new GunShop();
            gunShop.setName(SHOP_NAME);
        }
        return gunShop;
    }

    @Override
    public void run() {
        List<ProductCategory> productCategories = this.getAllProductCategories();
        populateProductTable(gunProdNames, productCategories, this);

        for (int i = 0; i < productCategories.size(); i++) {
            if (i == 1) {
                List<Product> someProducts = this.getProductsByProductCategory(productCategories.get(i));
                for (Product prod : someProducts) {
                    changProductStatus(prod, Product.Status.Absent.name());
                }
                System.out.println("Products Status were updated with " + Product.Status.Absent.name() + " for " + SHOP_NAME);
            } else {
                List<Product> someProducts = this.getProductsByProductCategory(productCategories.get(i));
                for (int x = 0; x < someProducts.size(); x += 2) {
                    changProductStatus(someProducts.get(x), Product.Status.Expected.name());
                }
                System.out.println("Products Status were updated with " + Product.Status.Expected.name() + " for " + SHOP_NAME);
            }
        }

        List<Product> availableProducts = this.getProductsByProductStatus(Product.Status.Available.name());

        for (Product prod : availableProducts) {
            changProductPrice(prod, new BigDecimal("1.2"));
        }
        System.out.println(SHOP_NAME + " WAS CLOSED");
        if (latch != null) {
            latch.countDown();
        }
    }
}
