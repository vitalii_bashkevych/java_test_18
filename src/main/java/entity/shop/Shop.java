package entity.shop;

import entity.Product;
import entity.ProductCategory;
import service.ProductCategoryService;
import service.ProductService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public abstract class Shop implements Runnable {

    int id;
    String name;
    String address;
    String phone;

    List<ProductCategory> productCategories;
    List<Product> products;

    ProductService productService = new ProductService();
    ProductCategoryService productCategoryService = new ProductCategoryService();

    CountDownLatch latch = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    //================================================================

    public List<Product> getAllProducts() {
        products = productService.fetchByShop(this);
        return products;
    }

    public List<Product> getProductsByProductCategory(ProductCategory productCategory) {
        return productService.fetchByProductCategory(productCategory);
    }

    public List<Product> getProductsByProductStatus(String status) {
        return productService.fetchByProductStatus(this, status);
    }

    public List<ProductCategory> getAllProductCategories() {
        productCategories = productCategoryService.fetchByShop(this);
        return productCategories;
    }

    public void addNewProduct(ProductCategory productCategory, String title, BigDecimal price, Product.Status status) {
        Product newProduct = new Product();
        newProduct.setTitle(title);
        newProduct.setProductCategory(productCategory);
        newProduct.setPrice(price);
        newProduct.setStatus(status);
        newProduct.setShop(this);

        productService.add(newProduct);
    }

    public void updateProduct(Product product) {
        productService.update(product);
    }

    public void deleteProduct(Product product) {
        productService.remove(product);
        product = null;
    }

    void populateProductTable(String[] prod, List<ProductCategory> cat, Shop shop) {
        Random random = new Random();

        for (int i = 0; i < prod.length; i++) {
            BigDecimal price = new BigDecimal(random.nextInt(9999999)).divide(new BigDecimal(100));
            int n = random.nextInt(cat.size());
            shop.addNewProduct(cat.get(n), prod[i], price, Product.Status.Available);
        }
    }

    void changProductPrice(Product product, BigDecimal coefficient) {
        BigDecimal price = product.getPrice();
        ProductService ps = new ProductService();
        price = price.multiply(coefficient).setScale(2, RoundingMode.UP);
        product.setPrice(price);
        ps.update(product);
    }

    void changProductStatus(Product product, String statusName) {
        ProductService ps = new ProductService();
        product.setStatus(Product.Status.valueOf(statusName));
        ps.update(product);
    }
}
