package entity;

import entity.shop.Shop;

public class ProductCategory {
    private int id;
    private String name;
    private Shop shop;

    public ProductCategory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                //"id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
