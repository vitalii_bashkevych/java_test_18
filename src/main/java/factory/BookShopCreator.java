package factory;

import entity.shop.BookShop;
import entity.shop.Shop;
import service.ShopService;

public class BookShopCreator implements ShopCreator {

    @Override
    public Shop createShop() {
        Shop shop = BookShop.getInstance();
        shop = new ShopService().fetchShop(shop);
        return shop;
    }
}
