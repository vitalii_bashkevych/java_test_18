package factory;

import entity.shop.GunShop;
import entity.shop.Shop;
import service.ShopService;

public class GunShopCreator implements ShopCreator {

    @Override
    public Shop createShop() {
        Shop shop = GunShop.getInstance();
        shop = new ShopService().fetchShop(shop);
        return shop;
    }
}
