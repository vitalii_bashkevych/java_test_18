package factory;

import entity.shop.Shop;

public interface ShopCreator {
    Shop createShop();
}
