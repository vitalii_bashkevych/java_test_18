import entity.shop.Shop;
import factory.BookShopCreator;
import factory.GunShopCreator;
import factory.ShopCreator;

import java.util.concurrent.CountDownLatch;

public class Start {

    public static void main(String[] args)throws InterruptedException {
        ShopCreator bShopCreator = new BookShopCreator();
        ShopCreator gShopCreator = new GunShopCreator();

        System.out.println("Start");

        final CountDownLatch latch = new CountDownLatch(2);

        Shop bookShop = bShopCreator.createShop();
        Shop gunShop = gShopCreator.createShop();

        Thread thread1 = new Thread(bookShop);
        Thread thread2 = new Thread(gunShop);

        bookShop.setLatch(latch);
        gunShop.setLatch(latch);

        thread1.start();

        try{
            Thread.sleep(10000);
        } catch (InterruptedException ex){
            System.out.println(ex.getMessage());
        }

        thread2.start();

        latch.await();
        System.out.println("=== ALL THREADS ARE STOPPED ===");

    }
}
