#--------------------------------------------------------------------------------
#--                     CREATE DB                                              --
#--------------------------------------------------------------------------------
CREATE DATABASE IF NOT EXISTS test_db;


#--------------------------------------------------------------------------------
#--                     CREATE USER                                            --
#--------------------------------------------------------------------------------
CREATE USER IF NOT EXISTS 'test'@'localhost'
  IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON test_db.* TO 'test'@'localhost';


#--------------------------------------------------------------------------------
#--                     CREATE TABLES                                          --
#--------------------------------------------------------------------------------
USE test_db;

CREATE TABLE IF NOT EXISTS shop (
  shop_id       INT         NOT NULL AUTO_INCREMENT,
  shop_name     VARCHAR(30) NOT NULL UNIQUE,
  shop_address  VARCHAR(60),
  contact_phone VARCHAR(14),
  PRIMARY KEY (shop_id)
)
  ENGINE = INNODB;
#--------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS product_category (
  product_category_id   INT         NOT NULL AUTO_INCREMENT,
  product_category_name VARCHAR(30) NOT NULL UNIQUE,
  shop_id               INT         NOT NULL,
  PRIMARY KEY (product_category_id),
  CONSTRAINT fk_shop FOREIGN KEY (shop_id)
  REFERENCES shop (shop_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = INNODB;
#--------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS status (
  status_id   INT AUTO_INCREMENT,
  status_name VARCHAR(30)UNIQUE,
  PRIMARY KEY (status_id)
)
  ENGINE = INNODB;
#--------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS product (
  product_id          INT NOT NULL AUTO_INCREMENT,
  product_title       VARCHAR(30) NOT NULL,
  product_price       DECIMAL(9, 2) NOT NULL,
  shop_id             INT NOT NULL,
  product_category_id INT NOT NULL,
  status           VARCHAR(30) NOT NULL,
  PRIMARY KEY (product_id),
  CONSTRAINT fk_shop_prod FOREIGN KEY (shop_id)
  REFERENCES shop (shop_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_product_category FOREIGN KEY (product_category_id)
  REFERENCES product_category (product_category_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_status FOREIGN KEY (status)
  REFERENCES status (status_name)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = INNODB;


#--------------------------------------------------------------------------------
#--                     POPULATE TABLES                                        --
#--------------------------------------------------------------------------------
INSERT INTO shop (shop_name, shop_address, contact_phone)
VALUES
  ('Book Shop', 'shop_address_1', '+380981234567'),
  ('Gun Shop', 'shop_address_2', '+380918234567');
#--------------------------------------------------------------------------------
INSERT INTO product_category (product_category_name, shop_id)
VALUES
  ('Ammunition', (select shop_id from shop where shop_name = 'Gun Shop')),
  ('Steel arms', (select shop_id from shop  where shop_name = 'Gun Shop')),
  ('Firearms', (select shop_id from shop where shop_name = 'Gun Shop')),
  ('Magazine', (select shop_id from shop where shop_name = 'Book Shop')),
  ('Book', (select shop_id from shop where shop_name = 'Book Shop'));
#--------------------------------------------------------------------------------
INSERT INTO status (status_id, status_name)
VALUES
  (1, 'Available'),
  (2, 'Absent'),
  (3, 'Expected');
#--------------------------------------------------------------------------------